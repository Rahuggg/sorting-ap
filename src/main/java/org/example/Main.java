package org.example;

public class Main {
    public static void main(String[] args) {
        Sorting sorting = new Sorting();

        sorting.sort(args);

        for (String s : args) {
            System.out.print(s + " ");
        }

    }
}